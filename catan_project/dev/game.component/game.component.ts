import {Component, OnInit, ChangeDetectionStrategy, Input} from 'angular2/core';


import {TilesService} from '../services/tiles.service';
import { Button } from '../data/button';
import { Road } from '../data/road';


@Component({
   
    selector: 'my-game-component',
    templateUrl: 'dev/game.component/game.component.html',
    styleUrls: ['../../src/css/game.component.css'],

    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [TilesService]
   
})



export class GameComponent implements OnInit{  

    @Input() players;
    @Input() gamerNumber;
    @Input() boardData;

    //segítség igénybevételeinek száma (plusz nyersanyag)

    szamlalo : number = 0;
    //aktuális játékos száma
    public currentPlayer : number;
    public buttonIndexValue : number;    
    public buttons = Array<Button>();
    public newButton : Button;
    public selectedValue :any;
    public button : Object;
    public valami: Array<number>;
    public circle: number;
    public isDisabled: boolean;

    //a táblán lévő számok
    lapszamok: Array<number> = new Array();
    //dobott szam
    currentDiceNumber : number;
     
    //mutathat város feliratot is
    showCity : boolean;

    //nyertes
    winner: string = 'eddig senkisem érte el az 5 pontot';


    playersData= Array<Object>();
    tilesColors: Array<string>; 
    tileNames: Array<string>;    
    refData: Object;

    //button image
    public isImage : boolean;
    public townShow = false;
    public cityShow = false;

    
       /*buttonokhoz*/

       buttonId : number;
       buttons_1: Array<Button> = new Array();
       buttons_2: Array<Button> = new Array();
       buttons_3: Array<Button> = new Array();
       buttons_4: Array<Button> = new Array();
       buttons_5: Array<Button> = new Array();
       buttons_6: Array<Button> = new Array();
       buttons_7: Array<Button> = new Array();
       buttons_8: Array<Button> = new Array();
       buttons_9: Array<Button> = new Array();

       currentButtonsArray: Array<Button> = new Array();
       buttonsArray: Array<Button> = new Array();

       //segéd tomb a gombok kezeléséhez
        roadColor12: string;
        roadColor14 :string;
        roadColor56: string;
        roadColor58 :string;
        roadColor54: string;
        roadColor910 :string;
        roadColor98: string;
        roadColor1124 :string;
        roadColor1110: string;
        roadColor1213 :string;
        roadColor122: string;
        roadColor315 :string;
        roadColor36: string;
        roadColor32 :string;
        roadColor717: string;
        roadColor710 :string;
        roadColor76: string;
        roadColor1419 :string;
        roadColor1415: string;
        roadColor1413: string;
        roadColor1621 :string;
        roadColor1617 :string;
        roadColor1615 :string;
        roadColor1823: string;
        roadColor1824: string;
        roadColor1817 :string;
        roadColor2223: string;
        roadColor2221 :string;
        roadColor2021: string;
        roadColor2019 :string;
   
       roadArray : Array<Road> = new Array();
       roadNIDs =[
            12,14,56,58,54,910,98,1124,1110,1213,122,315,36,32,717,710,76,1419,1415,1413,1621,1617,1615,1823,1824,1817,2223,2221,2021,2019
        ]

        //összes kör nyilvantarto
       sumCircle :number;

    constructor(private tilesService: TilesService){};


    ngOnInit(){
        
        //körszamlalo
         this.sumCircle = 0;

         this.showCity = false;

        this.buttonIndexValue = 0;
        this.buttonId = 1;

        //button képek
        this.isImage = false;
       


        this.buttons_1 = [
               new Button('button_left_top', ['', this.boardData[0].tilename, ''], [null,this.boardData[0].selectedNumber, null],1),
               new Button('button_left_bottom', ['', this.boardData[2].tilename, this.boardData[0].tilename], [null,this.boardData[2].selectedNumber, this.boardData[0].selectedNumber],2),
               new Button('button_center_bottom', [this.boardData[2].tilename, this.boardData[3].tilename, this.boardData[0].tilename], [this.boardData[2].selectedNumber,this.boardData[4].selectedNumber, this.boardData[0].selectedNumber],3),
               new Button('button_center_top', ['', this.boardData[0].tilename, ''], [null,this.boardData[0].selectedNumber, null],4)
        ];
        for(var i = 0; i<this.buttons_1.length; i++){
                this.buttonsArray.push(this.buttons_1[i]);
            }
        
        this.buttons_2=[
               new Button('button_left_top', [this.boardData[0].tilename, this.boardData[1].tilename, ''], [ this.boardData[0].selectedNumber,this.boardData[1].selectedNumber, null],5),
               new Button('button_left_bottom', [this.boardData[0].tilename, this.boardData[3].tilename, this.boardData[1].tilename], [this.boardData[0].selectedNumber,this.boardData[3].selectedNumber, this.boardData[1].selectedNumber],6),
               new Button('button_center_bottom', [this.boardData[3].tilename, this.boardData[4].tilename, this.boardData[1].tilename], [this.boardData[3].selectedNumber,this.boardData[4].selectedNumber, this.boardData[1].selectedNumber],7),
               new Button('button_center_top', ['', this.boardData[1].tilename, ''], [null,this.boardData[1].selectedNumber, null],8)
        ];
       for(var i = 0; i<this.buttons_2.length; i++){
                this.buttonsArray.push(this.buttons_2[i]);
            }


            this.buttons_3=[
               new Button('button_left_top', [this.boardData[1].tilename, '', ''], [this.boardData[1].selectedNumber,null, null],9),
               new Button('button_left_bottom', [this.boardData[1].tilename, this.boardData[3].tilename, ''], [this.boardData[1].selectedNumber,this.boardData[3].selectedNumber, null],10),
               new Button('button_center_bottom', [this.boardData[4].tilename, '', ''], [this.boardData[4].selectedNumber,null, null],11)
        ];
        for(var i = 0; i<this.buttons_3.length; i++){
                this.buttonsArray.push(this.buttons_3[i]);
            }
        
               this.buttons_4=[
               new Button('button_left_top', ['', this.boardData[2].tilename, ''], [null,this.boardData[2].selectedNumber, null],12),
               new Button('button_left_bottom', ['', '', this.boardData[2].tilename], [null,null, this.boardData[2].selectedNumber],13),
               new Button('button_center_bottom', ['', this.boardData[5].tilename, this.boardData[2].tilename], [null,this.boardData[5].selectedNumber, this.boardData[2].selectedNumber],14)
               
            ];
            for(var i = 0; i<this.buttons_4.length; i++){
                this.buttonsArray.push(this.buttons_4[i]);
            }

             this.buttons_5=[
               new Button('button_left_bottom', [this.boardData[2].tilename, this.boardData[5].tilename, this.boardData[3].tilename], [this.boardData[2].selectedNumber,this.boardData[5].selectedNumber, this.boardData[3].selectedNumber],15),
               new Button('button_center_bottom', [this.boardData[5].tilename, this.boardData[6].tilename, this.boardData[3].tilename], [this.boardData[5].selectedNumber,this.boardData[6].selectedNumber, this.boardData[3].selectedNumber],16)
               
            ];
            for(var i = 0; i<this.buttons_5.length; i++){
                this.buttonsArray.push(this.buttons_5[i]);
            }

            this.buttons_6=[
               new Button('button_left_bottom', [this.boardData[3].tilename, this.boardData[6].tilename, this.boardData[4].tilename], [this.boardData[3].selectedNumber,this.boardData[6].selectedNumber, this.boardData[4].selectedNumber],17),
               new Button('button_center_bottom', [this.boardData[6].tilename, '', this.boardData[4].tilename], [this.boardData[6].selectedNumber,null, this.boardData[4].selectedNumber],18)               
            ];
            for(var i = 0; i<this.buttons_6.length; i++){
                this.buttonsArray.push(this.buttons_6[i]);
            }
        //4.sor

        this.buttons_7=[
               new Button('button_left_bottom', ['', '', this.boardData[5].tilename], [null,null, this.boardData[5].selectedNumber],19),
               new Button('button_center_bottom', ['', '', this.boardData[5].tilename], [null,null, this.boardData[5].selectedNumber],20)             
               
            ];
            for(var i = 0; i<this.buttons_7.length; i++){
                this.buttonsArray.push(this.buttons_7[i]);
            }

            this.buttons_8=[
               new Button('button_left_bottom', [this.boardData[5].tilename, '', this.boardData[6].tilename], [this.boardData[5].selectedNumber,null, this.boardData[6].selectedNumber],21),
               new Button('button_center_bottom', ['', '', this.boardData[6].tilename], [null,null, this.boardData[6].selectedNumber],22)             
               
            ];
             for(var i = 0; i<this.buttons_8.length; i++){
                this.buttonsArray.push(this.buttons_8[i]);
            }
              this.buttons_9=[
               new Button('button_left_bottom', [this.boardData[6].tilename, '', ''], [this.boardData[6].selectedNumber,null, null],23),
               new Button('button_center_top', [this.boardData[6].tilename, '', ''], [this.boardData[4].selectedNumber,null, null],24)             
               
            ];
            for(var i = 0; i<this.buttons_9.length; i++){
                this.buttonsArray.push(this.buttons_9[i]);
            }
            

            this.currentButtonsArray =[
               new Button('button_left_bottom', [this.boardData[6].tilename, '', ''], [null,3, null],0),
                    
               
            ];

            
                         

        this.button= [
       {id: 1, element: 'Falu'},
       {id: 2, element: 'Varos'},
       ];
     this.selectedValue = null;

     this.valami=[
         1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
     ];
       
    // console.log('this.players',this.players);

        this.refData = [
            {name: '', index: 0},
            {name: 'Falu', index: '1'},
            {name: 'Város', index: '2'},

         ];
           //tegla: 0, szikla: 0, birka: 0, fa: 0, buza: 0
         for(var i=0; i<7; i++){
             this.lapszamok.push(this.boardData[i].selectedNumber);
         }

         console.log('this.lapszamok', this.lapszamok);


        
        
        this.tilesColors=this.tilesService.getTiles();
        this.tileNames=['Tégla', 'Szikla', 'Birka', 'Búza', 'Fa'];
       
        this.circle = 0;
        this.currentPlayer = 0;


       

        //utak
        for(var i = 0; i < this.roadNIDs.length; i++){
            this.roadArray[this.roadNIDs[i]] = new Road(this.roadNIDs[i], 'white');
        }
        
        
        
    }

    getStyle(c : number) {
        
    if(c == this.circle){
        this.isDisabled = true;
      return "#FFFACD";
    } else {
        this.isDisabled = false;
      return "";
    }

  }

    selectedDice(c: number){
        
        var rand = this.lapszamok[Math.floor(Math.random() * this.lapszamok.length)];
        this.currentDiceNumber = rand;
        this.setTile(c, this.currentDiceNumber);
    }

    setTile(c: number, d:number){
       for(var i = 0; i< this.buttonsArray.length; i++){
             for(var j=0; j<this.buttonsArray[i].numbersI.length; j++) {
                
               if(this.buttonsArray[i].numbersI[j] == d){                   
                  if(this.buttonsArray[i].playerId != 6){
                      for(var k = 0; k<5; k++ ){                          
                          if(this.players[this.buttonsArray[i].playerId].resources[k].name == this.buttonsArray[i].tilesI[j]){
                                  this.players[this.buttonsArray[i].playerId].resources[k].value++;                                  
                          }
                      }                      
                  }
               }
           }
       }
    }

    nextGamer(c: number){
       
         
          this.circle ++;
           this.currentPlayer = this.circle;
           this.currentDiceNumber = null;

          if(this.circle > 2){
              
              if(this.gamerNumber >3){

                  if(this.circle > 3){
                       this.circle = 0;
                       this.sumCircle++;
                  }
              }else{
                   this.circle = 0;
                   this.sumCircle++;
              }
             
          }
    }
    onChange(refDataValue) {
        if(refDataValue === 'Falu' ){
            this.townShow = true;
            this.cityShow = false;
        }else if(refDataValue === 'Város') {
           this.townShow = false;
           this.cityShow = true;
        }
        
    }   

    currentGamer(index:number, jatekosNumber: number){
      if(index == jatekosNumber){
        console.log(index);
        return true;

      }
      return false;
    }

   

     setButton(deviceValue, bNumber: number, cPlayer: number, group: number) {        
        
        //a köröket a sumCircle változo tartja nyílván

        if(group == 1){
            this.currentButtonsArray = this.buttons_1;
        }else if(group == 2){
            this.currentButtonsArray = this.buttons_2;
        }else if(group == 3){
            this.currentButtonsArray = this.buttons_3;
        }
        else if(group == 4){
            this.currentButtonsArray = this.buttons_4;
        }
        else if(group == 5){
            this.currentButtonsArray = this.buttons_5;
        }
        else if(group == 6){
            this.currentButtonsArray = this.buttons_6;
        }
        else if(group == 7){
            this.currentButtonsArray = this.buttons_7;
        }
        else if(group == 8){
            this.currentButtonsArray = this.buttons_8;
        }else if(group == 9){
            this.currentButtonsArray = this.buttons_9;
        }

       
       for(var i = 0; i<this. currentButtonsArray.length; i++){
          //aktuális gomb

                   if(this. currentButtonsArray[i].id == bNumber && (this.currentButtonsArray[i].playerId == cPlayer || this.currentButtonsArray[i].playerId==6) && this.currentButtonsArray[i].image !="url(./images/city.jpg)"){
                       
                       if(this.players[cPlayer].resources[0].value > 0 || 
                          this.players[cPlayer].resources[2].value > 0 ||
                          this.players[cPlayer].resources[3].value > 0 ||
                          this.players[cPlayer].resources[4].value > 0 || 
                          this.players[cPlayer].resources[1].value > 2 || 
                           this.players[cPlayer].resources[3].value > 1 
                         ) {
                      
                                      

                               //aktuális playernek a falujának vagy városának a száma, építés
                               if(deviceValue == 'Falu' && this.currentButtonsArray[i].image !="url(./images/town.jpg)"){
                                    this.currentButtonsArray[i].color = this.players[cPlayer].color;
                                       this.currentButtonsArray[i].playerId = cPlayer
                                   this.players[cPlayer].towns += 1;
                                  this.players[cPlayer].points +=1;

                                    this.currentButtonsArray[i].image="url(./images/town.jpg)";
                                    this.currentButtonsArray[i].imageBorder=this.players[cPlayer].color;
                                    this.currentButtonsArray[i].imageBorderStyle= 'groove';

                                   
                                    this.isImage = true;
                                    for (var i = 0; i<5; i++){
                                      if (this.players[cPlayer].resources[i].name == "brick" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }else if (this.players[cPlayer].resources[i].name == "wood" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }else if (this.players[cPlayer].resources[i].name == "sheep" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }else if (this.players[cPlayer].resources[i].name == "wheat" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }

                                    }
                                    for(var i = 0; i< this.currentButtonsArray[i].elements.length; i++){
                                        if(this.currentButtonsArray[i].elements[i].name == 'Varos'){
                                              this.currentButtonsArray[i].elements[i].visible == true;
                                        }                                    
                                    }
                                    if(this.players[cPlayer].points >=5){
                                       this.winner = this.players[cPlayer].name;
                                     }

                               }else if(deviceValue == 'Varos' && this.currentButtonsArray[i].image==="url(./images/town.jpg)"){
                                    this.players[cPlayer].cities +=2;
                                     this.players[cPlayer].towns -= 1;
                                    this.players[cPlayer].points +=2;
                                    this.currentButtonsArray[i].image="url(./images/city.jpg)";
                                    this.currentButtonsArray[i].imageBorder=this.players[cPlayer].color;
                                    this.currentButtonsArray[i].imageBorderStyle='groove';
                                    this.isImage = true;

                                    if(this.players[cPlayer].points >=5){
                                       this.winner = this.players[cPlayer].namez;
                                       this.isDisabled=false;
                                   }
                               }


                   }}
       }         


    }
   
    addTiles(cPlayer: number){
        this.szamlalo += 1;
        if(this.szamlalo <= 2){
            for (var i = 0; i<5; i++){
                this.players[cPlayer].resources[i].value++;
           }
        }
    }
    
    roadSelected(cPlayer: number, numberId: number){

        if(this.players[cPlayer].resources[0].value > 0 || 
                          this.players[cPlayer].resources[0].value > 0 &&
                          this.players[cPlayer].resources[2].value > 0 
                          
                         ) {

        for (var i = 0; i<5; i++){
                                      if (this.players[cPlayer].resources[i].name == "brick" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }else if (this.players[cPlayer].resources[i].name == "wood" ){
                                        this.players[cPlayer].resources[i].value--;
                                      }

                                    }

        if(this.roadArray[numberId].colorz=='white'){
               if(numberId == 12){
                 this.roadColor12 = this.players[cPlayer].color; 
                 this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 14){            
                    this.roadColor14 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 56){
                    this.roadColor56 =  this.players[cPlayer].color;
                     this.roadArray[numberId].colorz=this.players[cPlayer].color;           
               }else if(numberId == 58){
                 this.roadColor58 = this.players[cPlayer].color;

                 this.roadArray[numberId].colorz=this.players[cPlayer].color; 
               }else if(numberId == 54){            
                    this.roadColor54 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 910){
                    this.roadColor910 =  this.players[cPlayer].color;  
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 98){            
                    this.roadColor98 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1124){
                    this.roadColor1124 =  this.players[cPlayer].color; 
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;           
               }else if(numberId == 1110){
                 this.roadColor1110 = this.players[cPlayer].color; 
                 this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1213){            
                    this.roadColor1213 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 122){
                    this.roadColor122 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;  
               }else if(numberId == 315){            
                    this.roadColor315 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 36){
                    this.roadColor36 =  this.players[cPlayer].color; 
                    this.roadArray[numberId].colorz=this.players[cPlayer].color; 
               }else if(numberId == 32){            
                    this.roadColor32 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 717){
                    this.roadColor717 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;            
               }else if(numberId == 710){
                 this.roadColor710 = this.players[cPlayer].color; 
                 this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 76){            
                    this.roadColor76 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1419){
                    this.roadColor1419 =  this.players[cPlayer].color; 
                    this.roadArray[numberId].colorz=this.players[cPlayer].color; 
               }else if(numberId == 1413){            
                    this.roadColor1413 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1415){            
                    this.roadColor1415 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1621){
                    this.roadColor1621 =  this.players[cPlayer].color;  
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1617){            
                    this.roadColor1617 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1615){            
                    this.roadColor1615 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 1823){
                    this.roadColor1823 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;            
               }else if(numberId == 1824){
                    this.roadColor1824 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;            
               }else if(numberId == 1817){
                 this.roadColor1817 = this.players[cPlayer].color; 
                 this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 2223){            
                    this.roadColor2223 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 2221){
                    this.roadColor2221 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;  
               }else if(numberId == 2021){            
                    this.roadColor2021 =  this.players[cPlayer].color;
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }else if(numberId == 2019){
                    this.roadColor2019 =  this.players[cPlayer].color;  
                    this.roadArray[numberId].colorz=this.players[cPlayer].color;
               }   
       }


    }}

    
   
}