export class Player{  
   public id: number;
   public name:string;
   public color:string;
   public points:number;
   public longestRoad:boolean;
   public largestArmy:boolean;
   public devCards:Array<string>;
   public resources:Array<Object>;
  	public towns:number;
    public cities: number;


     constructor(public namez: string, public colorz: string) {
        this.name = namez;
        this.color = colorz;
        this.points = 0;
        this.longestRoad = false;
        this.largestArmy = false;
        this.devCards = [];
        this.resources = [
        {name: 'brick', value : 1},
        {name: 'ore', value : 1},
        {name: 'wood', value : 1},
        {name: 'wheat', value : 1},
        {name: 'sheep', value : 1} 
        ];
        // Trick to make array of 0's
       
        this.towns = 0;
        this.cities = 0;
    }
    

}