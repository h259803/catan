export class Button{  
    public class: string;
    public elements = Array<Object>();
    public tiles = Array<string>();
    public numbers = Array<number>();
    public id: number;
    public playerId : number;
    public visible: boolean;
    public color: string;
    public image : string;
    public isImage: boolean;
    public imageBorder: string;
    public imageBorderStyle: string;


    constructor(public classI : string, public tilesI :  Array<string>, public numbersI :  Array<number>, public idI : number){
     this.class=classI;
     this.elements  = [
         {id: 0, element: ''},
       {id: 1, element: 'Falu'},
       {id: 2, element: 'Varos'},
       ];
        /*arrays: leftTile , rightTile, bottomOrTopTile*/
     
     this.tiles = tilesI;
     this.id = idI;
     this.playerId = 6;
     this.visible = true;
     this.color = '#4682B4';
     this.image = 'none';
     this.isImage = false;
     this.imageBorder= 'none';
     this.imageBorderStyle= 'hidden';
    }

    
   
  }