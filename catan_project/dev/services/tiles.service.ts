import {Injectable} from 'angular2/core';

@Injectable()
export class TilesService {  
   
    private names = ['brick', 'ore', 'sheep', 'wheat', 'wood'];
    private names2 = ['brick', 'ore', 'sheep', 'wheat', 'wood'];
    private selectedNames : Array<string> = new Array();  
    private szamlalo = 0; 
    private tile : string; 
   getRandomString(){
   	this.szamlalo++;
   	if(this.szamlalo <=5){
   		let rndNum = Math.floor(Math.random() * this.names.length);
   		this.tile = this.names[rndNum];
   		this.names.splice(rndNum,1);
   	}else{
   		let rndNum = Math.floor(Math.random() * this.names2.length);
   		 this.tile = this.names2[rndNum]; 	
   }
   		return this.tile;
   }

   getTiles(){
   	 return this.names;
   }
}