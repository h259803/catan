import {Component} from 'angular2/core';
import {HelpComponent} from './help.component/help.component';
import {MainComponent} from './main.component/main.component';
import {  GameComponent } from './game.component/game.component'

@Component({
    selector: 'my-app', 
    templateUrl: 'dev/app.component.html',    
     directives: [MainComponent, GameComponent, HelpComponent]  
   
})
export class AppComponent { 
  jatekSzabalyok :boolean = false;
  i : number = 1;
  buttonText : string = "Jatekszabalyok";
  isStart :boolean = false;

  gameHelp(){
    this.i++;
   if(this.i%2 == 0) {
        this.jatekSzabalyok = true;
        this.buttonText = "Vissza"
    }else{
      this.jatekSzabalyok = false;
      this.buttonText = "Jatekszabalyok"
    }
   
  }

  getStart(){
    this.isStart=true;
  }
	
}
