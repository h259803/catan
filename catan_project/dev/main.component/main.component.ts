import {Component} from 'angular2/core';
import {TableComponent} from '../table.component';


@Component({
    selector: 'my-main-component',
 
    templateUrl: 'dev/main.component/main.component.html',


    styleUrls: ['../../src/css/main.component.css'],
     
    directives: [TableComponent]
})
export class MainComponent {
	
}
