import {Component} from 'angular2/core';
import {RandomTableComponent} from './tile.component/random.table';


@Component({
    selector: 'my-table-component',
    templateUrl: 'dev/table.component.html', 
    styleUrls: ['../src/css/app.css'],
    directives: [RandomTableComponent]  
   
})

export class TableComponent {
	}

