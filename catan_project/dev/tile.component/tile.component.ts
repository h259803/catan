
import {Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter} from 'angular2/core';

import {TilesService} from '../services/tiles.service';

@Component({
   
    selector: 'my-tile-component',
    templateUrl: 'dev/tile.component/tile.component.html',

    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['../../src/css/tile.component.css'],
    providers: [TilesService]

   
})
/*innen szerzem be az adatok a pályáról:
        - számok elhelyezkedését
        - tábla elemeit
        public selectedNumbers : Array<number> = new Array(); -ben vannak összegyűjtve

*/

export class TileComponent {
    disabled = false;

    tile: string;
    names_0: Array<String> = new Array();
    names_1: Array<String> = new Array();
    names_2: Array<String> = new Array();
    names_3: Array<String> = new Array();

    segedTomb: Array<String> = new Array();

    waters: Array<String> = new Array();
    refData: Object;


    public townShow = false;
    public cityShow = false;

//hatszögeken lévő képek és idjuk
    @Output() updated = new EventEmitter();
     public selectedTiles : Array<Object> = new Array();
    public tileNumber : number;
    public selectedNumbers : Array<number> = new Array();


    constructor(private tilesService: TilesService){
              
    };

    //kártyák
    ore: number;
    wheat: number;
    sheep: number;
    brick: number;
    wood: number;

    ngOnInit(){
        this.waters=['water','water'];
        
       
        for(var i = 0; i<2; i++){

           this.tile=this.tilesService.getRandomString();

           this.names_1.push(this.tile);  
            
        }
        for(var i = 0; i<2; i++){
           this.tile=this.tilesService.getRandomString();
           this.names_2.push(this.tile);   


        }
        for(var i = 0; i<1; i++){
           this.tile=this.tilesService.getRandomString();
           this.names_0.push(this.tile);       
        }

         for(var i = 0; i<2; i++){
           this.tile=this.tilesService.getRandomString();
           this.names_3.push(this.tile);       
        }
         


        this.refData = [
            {name: '', index: 0},
            {name: 'Falu', index: '1'},
            {name: 'Város', index: '2'},

         ];

         this.tileNumber = 0;
        
    }

     ngAfterViewInit() {
       
         var j = 0;
         for(var i = 0; i<this.names_1.length; i++){
             this.tileNumber ++;
            this.selectedTiles.push({tileId : this.tileNumber, tilename : this.names_1[i] , selectedNumber :   this.selectedNumbers[j]});
            j++;

         }
          for(var i = 0; i<this.names_0.length; i++){             
            this.tileNumber ++;
            this.selectedTiles.push({tileId : this.tileNumber, tilename : this.names_0[i] , selectedNumber :   this.selectedNumbers[j]});
             j++;
         }

          for(var i = 0; i<this.names_2.length; i++){
             this.tileNumber ++;
            this.selectedTiles.push({tileId : this.tileNumber, tilename : this.names_2[i] , selectedNumber :   this.selectedNumbers[j]});
             j++;
         }

             for(var i = 0; i<this.names_3.length; i++){
             this.tileNumber ++;
               this.selectedTiles.push({tileId : this.tileNumber, tilename : this.names_3[i] , selectedNumber :   this.selectedNumbers[j]});
             j++;
         }
         //itt adom át a tábla adatokat a többi komponensnek
         this.updated.emit(this.selectedTiles);

       }


    onChange(refDataValue) {
        if(refDataValue === 'Falu' ){
            this.townShow = true;
            this.cityShow = false;
        }else if(refDataValue === 'Város') {
           this.townShow = false;
           this.cityShow = true;
        }
        
    }   
    
   getRandomNumber(){
       let rndNum = Math.floor(Math.random() * 6) + 1;
       this.selectedNumbers.push(rndNum);
       
       return rndNum;
   } 


}