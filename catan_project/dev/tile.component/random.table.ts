import {Component, Output, EventEmitter } from 'angular2/core';
import {TileComponent} from './tile.component';
import {  GameComponent } from '../game.component/game.component';
import {Player} from '../data/player';

@Component({
    selector: 'my-random-table-component',
    templateUrl: 'dev/tile.component/random.table.html',
    styleUrls: ['../../src/css/start.page.css'],
    directives: [TileComponent, GameComponent],
   
      	
})

/*innen szerzem a következő adatokat:
    -játékosok nevei, színei
    -játékosok száma
*/
export class RandomTableComponent {
  boardData: Array<Object> = new Array;

  firstPlayerClick : boolean;
  secondPlayerClick : boolean;
  thirdPlayerClick : boolean;
  fourPlayerClick : boolean;



  firstColor : string;
  secondColor : string;
  thirdColor : string;
  fourColor : string;

  gamerNumber : number;

	myAlert: string;
	isStartGame: boolean;
	startPlayerNames: boolean;
	isHaromJatekos : boolean;
	isNegyJatekos: boolean;
  playersData = Array<Object>();
  public players = Array<Player>();

    
    

	colors=[
            {id: 0 ,name: 'red', text: 'Piros' },
            {id: 1 ,name: 'yellow', text: 'Sárga'},
            {id: 2 ,name: 'green', text: 'Zöld'},
            {id: 3 ,name: 'blue', text: 'Kék'} 
        ];

    isValidStartClick : boolean;

        constructor(){

             
        };

	ngOnInit(){
    		this.startPlayerNames=false;
    		this.isStartGame= false;
    		this.isHaromJatekos= false;
    		this.isNegyJatekos= false;
        this.isValidStartClick=false;

        this.firstPlayerClick = false;
        this.secondPlayerClick = false;
        this.thirdPlayerClick = false;
        this.fourPlayerClick = false;

        
	}
  //itt veszem át a tábláról szükséges tudnivalókat
  updated(inputData){
    this.boardData = inputData;
  }

	freshPage(){
 		window.location.reload();
	}

	startSetPlayersName(){
		this.startPlayerNames=true;
	}
	startGame(){
		
		this.isStartGame = true;
        
		
	}

	playerNumber(number: number){

        this.gamerNumber = number;
		if(number == 3){
			this.isHaromJatekos= true;
		}

		if(number == 4){
		this.isNegyJatekos= true;
			
		}
	}
	colorSelect(colorName: string, playerName: string, playerNumber: number){
       
      
       this.players.push(new Player(playerName, colorName));
       this.playersData.push({number: playerNumber, name: playerName, color : colorName  });
       
        if(playerNumber == 1){
             this.firstPlayerClick = true;
              this.firstColor  = colorName;
       
         }

        if(playerNumber == 2){
            this.secondPlayerClick = true;
             this.secondColor = colorName;
   
         }
     

       if(playerNumber == 3){
             this.thirdPlayerClick = true;
                  this.thirdColor = colorName;
    
          }
     

       if(playerNumber == 4){
             this.fourPlayerClick = true;
                 this.fourColor = colorName;
          }
     
     
		
      this.colors = this.colors.filter(function(colors){
               return colors.name !== colorName;
        });

      if(this.gamerNumber == 4){
         if( this.firstPlayerClick == true && this.secondPlayerClick == true && this.thirdPlayerClick == true && this.fourPlayerClick == true ){
            this.isValidStartClick = true;
        }
      }else if(this.gamerNumber == 3 ){
         if( this.firstPlayerClick == true && this.secondPlayerClick == true && this.thirdPlayerClick == true){
            this.isValidStartClick = true;
        }
      }
       
	}
}